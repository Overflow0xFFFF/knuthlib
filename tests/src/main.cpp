/**
 * Copyright (C) 2019 Joshua Ford
 *
 * This file is made available under the BSD 3-Clause license. See LICENSE at
 * the root of this project for details.
 */
#include <gtest/gtest.h>

/****************************************************************************/
int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

