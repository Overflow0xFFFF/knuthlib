knuthlib
========

An open source C library implementing some of the data structures and
algorithms found in Donald Knuth's _The Art of Computer Programming_.

