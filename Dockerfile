FROM alpine:3.11
MAINTAINER Joshua Ford <joshua.ford@protonmail.com>

RUN apk update && apk --no-cache add \
    binutils \
    build-base \
    ca-certificates \
    clang \
    cmake \
    git \
    gradle \
    make

